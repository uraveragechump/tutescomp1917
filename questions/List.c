#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "List.h"

// Taken from wk8 tutorial code
// Dong.
/*
typedef struct node * Lnode;

struct node {
    int val;
    Lnode next;
}
*/
// Initialises a newNode with memory allocation and value-setting
Lnode newNode(int value) {
    Lnode newNode = malloc (sizeof(Lnode));
    if (newNode == NULL) { 
        printf("no memory\n");
        exit(1);
    }
    newNode->val = value;
    newNode->next = NULL;
    return newNode;
}

// Returns the head (front) of the new list
Lnode prepend(Lnode head, Lnode new) {
    new->next = head;
    return new;
}

// Returns the head (front) of the new list;
Lnode append(Lnode head, Lnode new) {
    if (head == NULL) {
        return new;
    }

    Lnode curr = head;

    // Last element of a list node will have ->next pointing nowhere (NULL)
    for (curr = head; curr->next != NULL; curr = curr->next) {
        // Do nothing, just set curr to last item
    }
    curr->next = new;
    //printf("[%d]->[%d]\n", curr->val, curr->next->val);
    return head;
}

// WhileLoop print
void printList(Lnode head) {
    int i = 0;
    Lnode curr = head;

    // Iterate through list until end is found (where curr == NULL)
    while (curr != NULL && i < 20) {
        printf("(%d)->", curr->val);
        curr = curr->next;
        i++;
    }

    printf("(x)\n");
}
// ForLoop print
void printListF(Lnode head) {
    Lnode curr = head;

    for (curr = head; curr != NULL; curr = curr->next) {
        printf("[%d]->", curr->val);
    }

    printf("(x)\n");
}
// Returns new head of list (always reallocate your head pointer to this return)
Lnode deleteNode(Lnode head, int val) {
    // Return nothing if nothing to delete
    if (head == NULL) {
        return NULL;
    }

    Lnode curr = head;
    Lnode prev = NULL;

    // Iterate through list, checking for values
    for (curr = head; curr != NULL; curr = curr->next) {
        if (curr->val == val) {
            // free node and create connection from prev to next
            // a. first item to be removed
            // b. only item to be removed
            // c. non-first item to be removed
            if (prev == NULL && curr->next != NULL) {
                /*
                head = head->next;
                curr = NULL;
                free(curr);
                */
                curr = head->next;
                head = NULL;
                free(head);
                return curr;
            }
            else if (prev == NULL && curr->next == NULL) {
                head = NULL;
                free(head);
                return NULL;
            }
            prev->next = curr->next;
            curr = NULL;
            free(curr);
            return head;
        }
        prev = curr;
    }
    
    // Removed nothing, return list
    return head;
    
}

// Strategy: Get the last node in the list with head L, and link it to head of M
// Exactly the same as append!
Lnode concat(Lnode L, Lnode M) {

    return append(L, M);
    /*
    Lnode curr = L;
    for (curr = L; curr->next != NULL; curr = curr->next) {
        // Do nothing, get curr to last node in list with head L
    }
    curr->next = M;
    return L;
    */
}

Lnode reverse(Lnode head) {
    Lnode prev = NULL;
    Lnode curr = head;
    Lnode next = curr->next;

    for (curr = head; curr != NULL; curr = next) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
    }
    return prev;
}

int rLength(Lnode head) {
    if (head == NULL) {
        return 0;
    }
    return rLength(head->next) + 1;
}
void recPrintRev(Lnode head) {
    if (head == NULL) {
        return;
    }
    recPrintRev(head->next);
    printf("<-(%d)", head->val);

}

void rPrintRev(Lnode head) {
    recPrintRev(head);
    printf("\n");
}

int sumOfTwo(Lnode head, int k) {
    Lnode curr1 = head;
    for (curr1 = head; curr1 != NULL; curr1 = curr1->next) {
        Lnode curr2 = curr1;
        for (curr2 = curr1; curr2 != NULL; curr2 = curr2->next) {
            if (curr2->val + curr1->val == k) {
                return 1;
            }
        }
    }
    return 0;
}

void swap(int * val1, int * val2) {
    int temp = *val1;
    *val1 = *val2;
    *val2 = temp;
}

// This function will push the fromIndex node *after* the toIndex node, and retain the toIndex position
// I.e. the node previously at fromIndex will now be at toIndex
// The node previously at toIndex will be at toIndex-1
Lnode moveNode(Lnode head, int fromIndex, int toIndex) {
    // Ensure index1 < index2
    if (fromIndex < toIndex) {
        swap(&fromIndex, &toIndex);
    }
    
    Lnode from = head;
    Lnode fPrev = NULL;

    int i = 0;
    for (i = 0; i < fromIndex; i++) {
        fPrev = from;
        from = from->next;
    }

    // curr keeps track of the node *before* toIndex
    Lnode curr = head;
    for (i = 0; i < toIndex; i++) {
        curr = curr->next;
    }

    // This method cleanly handles cases where nodes are next to each other (i.e. Abs(fromIndex - toIndex) == 1)
    if (from == 0 || fPrev == NULL) {
        // Dealing with the head node:
        // - need to repoint head, 
        // - don't need to keep track of prev1
        head = from->next;
        from->next = curr->next;
        curr->next = from;
    }
    else {
        // Not dealing with the head node:
        // - Keep track of prev1
        fPrev->next = from->next; 
        from->next = curr->next;
        curr->next = from;
    }
    return head;
}

// Assumes node1 is earlier than node2
// Helper function - no error checking done
Lnode swapNodes(Lnode head, Lnode prev1, Lnode prev2, Lnode node1, Lnode node2) {

    if (node1 == head) {
        
        // First allocate prev->next pointers; still have pointers on currs
        // Prev1 is NULL - don't do anything with it
        // Set new head
        head = node2;
        prev2->next = node1;

        // Do a swap with node1->next and node2->next;
        Lnode temp = node1->next;
        node1->next = node2->next;
        node2->next = temp;
        return head;
    }
    else {
        // First allocate prev->next pointers; still have pointers on nodes
        prev1->next = node2;
        prev2->next = node1;

        // Do a swap with node1->next and node2->next;
        Lnode temp = node1->next;
        node1->next = node2->next;
        node2->next = temp;
    }

    return head;

}   

// Returns new head of list
Lnode swapNodesByIndex(Lnode head, int index1, int index2) {
    int len = rLength(head);
    // If either index is greater than length or negative, one of the nodes don't exist
    // If the indices are equal, no swap performed
    if (index1 > len || index2 > len || index1 < 0 || index2 < 0 || index1 == index2) {
        return head;
    }
    
    // Ensure index1 < index2
    if (index2 < index1) {
        swap(&index1, &index2);
    }

    // Initialise node pointers to point to respective indices
    // Node 1 is the node at new index1, prev1 points before node1
    // Node 2 is the node at new index2, prev2 points before node2
    Lnode node1 = head;
    Lnode node2 = head;
    Lnode prev1 = NULL;
    Lnode prev2 = NULL;

    int i = 0;
    for (i = 0; i < index1; i++) {
        prev1 = node1;
        node1 = node1->next;
    }
    i = 0;
    for (i = 0; i < index2; i++) {
        prev2 = node2;
        node2 = node2->next;
    }

    // If you only care about swapping the values (which would be in most real cases)
    //swap(&node1->val, &node2->val);
    
    // If you care about swapping the actual nodes ;-;
    // Note that our helper function only really needed to pass in the prevs to figure out curr.
    head = swapNodes(head, prev1, prev2, node1, node2);

    return head;

}

// Helper function for removeDupes
Lnode deleteAllNodes(Lnode head, int val) {
    int len = rLength(head);
    int i;
    // Dirty naive method to remove all nodes of a particular value
    // This works because at most there can only be len dupes
    // deleteNode won't break if given a non-existent value
    for (i = 0; i < len; i++) {
        head = deleteNode(head, val);
    }
    return head;
}

Lnode removeDupes(Lnode head) {
    Lnode curr = head;
    
    for (curr = head; curr != NULL; curr = curr->next) {
        // Remove all subsequent nodes with the same value
        curr->next = deleteAllNodes(curr->next, curr->val);
    }
    return head;
}
