#include <stdio.h>
#include <malloc.h>
#include "List.h"

// Modified from wk8 code
// Dong.
//
void swapNodesByIndexTest();
void removeDupesTest();

#define TEST 1

int main() {

    if (TEST == 0) {
        swapNodesByIndexTest();
    }
    if (TEST == 1) {
        removeDupesTest();
    }


    return 0;
}

void swapNodesByIndexTest() {

    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            Lnode first = newNode(0);
            Lnode second = newNode(1);
            Lnode third = newNode(2);
            Lnode fourth = newNode(3);
            Lnode fifth = newNode(4);

            append(first, second);
            append(first, third);
            append(first, fourth);
            append(first, fifth);

            printf("Before Swap:\n");
            printList(first);

            first = swapNodesByIndex(first, i, j);

            printf("After Swap (between index %d and %d):\n", i, j);
            printList(first);
            printf("\n");

        }
    }
}

void removeDupesTest() {
    Lnode first = newNode(4);
    Lnode second = newNode(1);
    Lnode third = newNode(1);
    Lnode fourth = newNode(3);
    Lnode fifth = newNode(4);

    append(first, second);
    append(first, third);
    append(first, fourth);
    append(first, fifth);

    printf("Before Dupe Removal:\n");
    printList(first);

    first = removeDupes(first);

    printf("After Dupe Removal:\n");
    printList(first);
    printf("\n");

}
