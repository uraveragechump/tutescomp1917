#include <stdio.h>

#define MAX_LEN 100

int main() {
    char str[MAX_LEN];
    // Deprecated function
    //gets(str);
    //printf("%s\n", str);

    fgets(str, MAX_LEN, stdin);
    printf("%s", str);
    return 0;
}
