#include <stdio.h>
#include <malloc.h>

// Modified from wk8 tutorial code 
// Dong.

typedef struct node * Lnode;

struct node {
    int val;
    Lnode next;
};

// Initialises a newNode with memory allocation and value-setting
Lnode newNode(int value);

// Returns the head (front) of the new list
Lnode prepend(Lnode head, Lnode new);

// Returns the head (front) of the new list;
Lnode append(Lnode head, Lnode new);

// WhileLoop print
void printList(Lnode head);

// ForLoop print
void printListF(Lnode head);

// Returns new head of list (always reallocate your head pointer to this return)
Lnode deleteNode(Lnode head, int val);

// Combines two lists together by appending one list onto another
Lnode concat(Lnode L, Lnode M);

// Reverses a list starting with head
Lnode reverse(Lnode head);

int rLength(Lnode head);

void rPrintRev(Lnode head);

int sumOfTwo(Lnode head, int k);

Lnode swapNodesByIndex(Lnode head, int index1, int index2);

Lnode removeDupes(Lnode head);
