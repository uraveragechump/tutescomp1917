#include <stdio.h>
#include "Point.h"

// Compile with gcc -Wall -Werror -o testPoint testPoint.c Point.c Point.h
int main() {

    Point p = newPoint(0,0);
    shiftPoint(p,1,-1);

    printf("x value is: %f\n", getX(p));
    printf("y value is: %f\n", getY(p));
    destroyPoint(p);

    printUtil();

    return 0;
}
