struct node {
    int val;
    Lnode next;
}

Lnode concat(Lnode M, Lnode N) {

    Lnode curr = M;

    while (curr->next != NULL) {
        curr = curr->next;
    }
    curr->next = N;
    return M;

    
}
