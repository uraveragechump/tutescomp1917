#include <stdio.h>
#include <malloc.h>

typedef struct node * Lnode;
typedef struct list * List;

struct node {
    int val;
    Lnode next;
};

struct list {
    Lnode head;
    int numNodes;
};

// Initialises a newNode with memory allocation and value-setting
List newList();

// Returns the head (front) of the new list
List prepend(List l, int new);

// Returns the head (front) of the new list;
List append(List l, int new);

// WhileLoop print
void printList(List l);

// Returns new head of list (always reallocate your head pointer to this return)
List deleteNode(List l, int val);

// Appends one list to another
List concat(List l, List m);

// Reverses a list
List reverse(List l);
