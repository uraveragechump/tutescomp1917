#include <stdio.h>
#include "Point.h"

struct point {
    double x;
    double y;
};

Point newPoint(double x, double y) {
    Point p = malloc(sizeof(Point));
    if (p == NULL) {
        exit(1);
    }
    p->x = x;
    p->y = y;
    return p;
}

void destroyPoint(Point p) {
    free(p);
}

void setPoint(Point p, double x, double y) {
    p->x = x;
    p->y = y;
}

double getX(Point p) {
    return p->x;
}

double getY(Point p) {
    return p->y;
}

void shiftPoint(Point p, double dx, double dy) {
    /*
   p->x = p->x + dx;
   p->y += dy;
   */

   setPoint(p, getX(p) + dx, getY(p) + dy); 
}

void printUtil() {
    printf("test\n");
}

