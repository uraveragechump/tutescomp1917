#include <stdio.h>

typedef struct point * Point;

Point newPoint(double x, double y);
void destroyPoint(Point p);
double getX(Point p);
double getY(Point p);
void setPoint(Point p, double x, double y);
void shiftPoint(Point p, double xDist, double yDist);
