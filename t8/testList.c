#include <stdio.h>
#include <malloc.h>
#include "List.h"


int main() {

    /*
    Lnode one = newNode(1);
    printf("%p\n", one);
    deleteNode(one, 1);
    Lnode two = newNode(2);
    printf("%p\n", two);
    */
    //Lnode three = newNode(3);
    Lnode head = newNode(1);
    //printf("%p\n", head);
    Lnode new = newNode(3);
    Lnode last = newNode(4);
    
    // This line sets "head" at the front of the list after prepending new onto list head.
    printf("prepend 3 onto list (1)->(x), resets head\n");
    head = prepend(head, new);
    // Print's the list - these two lines will have diff output if head is not set properly
    //printList(new);
    printList(head);

    // This line doesn't, so new is the new front of the list.
    //printf("prepend 3 onto 1, doesn't reset head\n");
    //prepend(head, new);

    // Appends last to end of list pointed to by head
    printf("append 4 onto list (3)->(1)->(x), head doesn't need to be reset\n");
    append(head, last);
    printList(head);

    printf("delete the first 3 in list (3)->(1)->(4)->(x), resets head\n");
    head = deleteNode(head, 3);
    printList(head);

    printf("delete the first 4 in list (1)->(4)->(x), resets head\n");
    head = deleteNode(head, 4);
    printList(head);

    printf("delete the first 1 in list (1), resets head\n");
    head = deleteNode(head, 1);
    printList(head);

    int i = 0;
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    // Sets up a list (1)->(2)->(3)->(4)
    Lnode one = newNode(1);
    Lnode two = newNode(2);
    Lnode three = newNode(3);
    printf("%p\n", three);
    printf("%p\n", three->next);
    printf("%d\n", ++i);
    printList(three);
    Lnode four = newNode(4);
    printf("%d\n", ++i);

    one = append(one, two);
    printf("%d\n", ++i);
    printList(one);
    one = append(one, three);
    //return 1;
    printList(one);
    printf("%d\n", ++i);
    one = append(one, four);
    printf("%d\n", ++i);

    // Sets up a list (5)->(6)->(7)->(8)
    Lnode five = newNode(5);
    Lnode six = newNode(6);
    Lnode seven = newNode(7);
    Lnode eight = newNode(8);
    printf("%d\n", ++i);

    append(five, six);
    append(five, seven);
    append(five, eight);
    printf("%d\n", ++i);

    printf("List 1 starting with one:\n");
    printList(one);

    printf("List 2 starting with five:\n");
    printList(five);

    one = concat(one, five);
    printf("Concatenated Lists 1, 2:\n");
    printList(one);

    Lnode newhead = reverse(one);
    printf("Reversed List:\n");
    printList(newhead);

    return 0;
}

