#include <stdio.h>
#include <malloc.h>
#include "List2.h"


int main() {
    List list = newList();
    prepend(list, 1);
    
    printf("prepend 3 onto list (1)->(x), resets head\n");
    prepend(list, 3);
    printList(list);

    // Appends last to end of list pointed to by head
    printf("append 4 onto list (3)->(1)->(x), head doesn't need to be reset\n");
    append(list, 4);
    printList(list);

    printf("delete the first 3 in list (3)->(1)->(4)->(x), resets head\n");
    list = deleteNode(list, 3);
    printList(list);

    printf("delete the first 4 in list (1)->(4)->(x), resets head\n");
    list = deleteNode(list, 4);
    printList(list);

    printf("delete the first 1 in list (1), resets head\n");
    list = deleteNode(list, 1);
    printList(list);

    return 0;
}

