

typedef struct node * Node;

struct node {
    int val;
    Node next;
}
