#include <stdio.h>
#include <malloc.h>

typedef struct speedingTicket * SpeedingTicket;
typedef struct date * Date;

typedef struct date Date;

Date newDate(int year, int month, int day);
SpeedingTicket newSpeedingTicketD(Date d);
SpeedingTicket newSpeedingTicketI(int year, int month, int day);

struct speedingTicket {
    Date date;
};

struct date {
    int year;
    int month;
    int day;
};

int main() {
    // Make array of Speeding Tickets - allocates space for 10 pointers.
    SpeedingTicket * speedArr = malloc (sizeof(SpeedingTicket *) * 10);

    // Fill each element in array with a new SpeedingTicket, with year, month, and day as i;
    int i;
    for (i = 0; i < 10; i++) {
        speedArr[i] = newSpeedingTicketI(i, i, i);
    }

    // Prints out the year of the second SpeedingTicket (i.e. index 1) in the array.
    printf("Year: %d\n", (*((*speedArr[1]).date)).year);
    printf("Year: %d\n", speedArr[1]->date->year);
    return 0;
}

Date newDate(int year, int month, int day) {
    Date this = malloc (sizeof(SpeedingTicket));
    this->year = year;
    this->month = month;
    this->day = day;
    return this;
}

SpeedingTicket newSpeedingTicketD(Date d) {
    SpeedingTicket this = malloc (sizeof(SpeedingTicket));
    this->date = d;
    return this;
}

SpeedingTicket newSpeedingTicketI(int year, int month, int day) {
    SpeedingTicket this = malloc (sizeof(SpeedingTicket));
    Date d = newDate(year, month, day);
    this->date = d;
    return this;
}


