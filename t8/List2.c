#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include "List2.h"

// Initialises a newNode with memory allocation and value-setting

List newList() {
    List newList = malloc(sizeof(List));
    if (newList == NULL) {
        printf("No memory!\n");
        exit(1);
    }
    newList->numNodes = 0;
    return newList;
}

Lnode newNode(int num) {
    Lnode new = malloc(sizeof(Lnode));
    if (new == NULL) {
        printf("No memory!\n");
        exit(1);
    }
    new->val = num;
    return new;
}

// Returns the head (front) of the new list
List prepend(List l, int num) {
    Lnode new = newNode(num);
    // If list is not empty, prepend new onto existing list
    if (l->numNodes != 0) {
        new->next = l->head;
    }
    // set head pointer to new;
    l->head = new;
    l->numNodes++; // Function always adds one element to list
    return l;
}

// Returns the head (front) of the new list;
List append(List l, int num) {
    /*
    if (l->numNodes == 0) {
        prepend(l, num);
        return l;
    }
    */
    Lnode new = newNode(num);
    Lnode curr = l->head;

    // Last element of a list node will have ->next pointing nowhere (NULL)
    for (curr = l->head; curr->next != NULL; curr = curr->next) {
        // Do nothing, just set curr to last item
    }
    curr->next = new;
    l->numNodes++; // Function always adds one element to list
    return l;
}

void printList(List l) {
    Lnode curr = l->head;
    printf("%-25s", "ForLoop print: ");

    for (curr = l->head; curr != NULL; curr = curr->next) {
        printf("[%d]->", curr->val);
    }

    printf("(x)\n");
}
// Returns new head of list (always reallocate your head pointer to this return)
List deleteNode(List l, int val) {

    if (l->numNodes == 0) {
        return l;
    }

    Lnode curr = l->head;

    // If first one is to be removed
    if (l->head->val == val) {
        // Free head and return the next element
        curr = l->head->next;
        l->head = NULL;
        free(l->head);
        l->head = curr;
        l->numNodes--;
        return l;
    }

    Lnode prev = l->head;
    // Iterate through list, checking for values
    for (curr = l->head->next; curr != NULL; curr = curr->next) {
        if (curr->val == val) {
            // free node and create connection from prev to next
            prev->next = curr->next;
            curr = NULL;
            free(curr);
            l->numNodes--;
            return l;
        }
        prev = curr;
    }
    
    // Removed nothing, return list
    return l;
    
}

// Left as an exercise
List concat(List l, List m) {
    return l;
}

// Left as an exercise
List reverse(List l) {
    return l;
}
