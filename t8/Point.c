#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "Point.h"

struct point {
    double x;
    double y;
};

Point newPoint(double x, double y) {
    Point this = malloc(sizeof(Point));
    this->x = x;
    this->y = y;
    return this;
}

void destroyPoint(Point p) {
    free(p);
}

double getX(Point p) {
    return p->x;
}

double getY(Point p) {
    return p->y;
}

void setPoint(Point p, double x, double y) {
    p->x = x;
    p->y = y;
}

void shiftPoint(Point p, double dx, double dy) {
    p->x += dx;
    p->y += dy;
}
