#include <stdio.h>
#include <string.h>
#include <malloc.h>

//
// Written as an amendment to Wednesday t10.
// Dong.

typedef struct stringthing * Stringthing;

struct stringthing {
    int len;
    char * string;
};

Stringthing newStringthing(char * string, int len) {
    Stringthing new = malloc(sizeof(Stringthing));
    new->len = len;
    new->string = malloc (sizeof(char) * len);
    strcpy(new->string, string);
    return new;
}

void freeStringthing(Stringthing s) {
    free(s->string); // It's been malloc'ed separately, need to free first
    free(s);
}

void printStringthing(Stringthing s) {
    printf("Len: %d\n", s->len);
    printf("String: %s\n", s->string);
}

int main() {
    
    char * sample = "this is a sample string";
    int len = strlen(sample);
    Stringthing str = newStringthing(sample, len);
    printStringthing(str);
    freeStringthing(str);
    return 0;
}
