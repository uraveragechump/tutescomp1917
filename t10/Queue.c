#include <stdio.h>
#include <malloc.h>

typedef struct lnode * Lnode;

struct lnode {
    Lnode next;
    int val;
};

typedef struct queue * Queue;

struct queue{
    Lnode head;
    int num_nodes;
};

Queue newQueue() {
    Queue new = malloc(sizeof(Queue));
    new->num_nodes = 0;
    return new;
}

Lnode newNode(int val) {
    Lnode new = malloc (sizeof(Lnode));
    new->val = val;
    new->next = NULL;
    return new;
}

/*
void enqueue(Queue q, int val) {
    Lnode new = newNode(val);
    
    if (q->head == NULL) {
        q->head = new;
        q->num_nodes++;
        return;
    }

    new->next = q->head;
    q->head = new;
    q->num_nodes++;
}

void dequeue(Queue q) {

    if (q == NULL || q->head == NULL) {
        return;
    }

    Lnode prev = NULL;
    Lnode curr = q->head;
    while (curr->next != NULL) {
        //printf("iterate\n");
        prev = curr;
        curr = curr->next;
    }

    if (curr == q->head) {
        q->head = NULL;
        // perform delete - make sure it's an empty list
    }
    else {
        prev->next = NULL;
    }

    curr = NULL;
    free(curr);
    q->num_nodes--;
    // perform delete - but not an empty list
}
*/

void enqueue(Queue q, int val) {
    if (q == NULL) {
        return;
    }

    if (q->num_nodes == 0 || q->head == NULL) {
        Lnode new = newNode(val);
        q->head = new;
        q->num_nodes++;
        return;
    }

    Lnode curr = q->head;
    Lnode prev = NULL;
    for (curr = q->head; curr != NULL; curr = curr->next) {
        prev = curr;
    }

    Lnode new = newNode(val);
    prev->next = new;
    q->num_nodes++;
}

void dequeue(Queue q) {
    if (q == NULL || q->head == NULL) {
        return;
    }

    Lnode oldHead = q->head;
    Lnode newHead = q->head->next;
    if (q->num_nodes > 1) {
        q->head = newHead;
        oldHead = NULL;
        free(oldHead);
        q->num_nodes--;
        return;
    }
    q->head = NULL;
    free(q->head);
    q->num_nodes--;
}

void printQueue(Queue q) {
    Lnode curr = q->head;
    printf("Numnodes: %d\n", q->num_nodes);
    for (curr=q->head; curr!= NULL; curr = curr->next) {
        printf("(%d)->", curr->val);
    }
    printf("(x)\n");
}

int main() {
    
    Queue q = newQueue();

    enqueue(q, 1);

    printQueue(q);

    enqueue(q, 2);

    printQueue(q);

    dequeue(q);

    printQueue(q);

    dequeue(q);

    printQueue(q);

    dequeue(q);

    printQueue(q);

    return 0;
}
