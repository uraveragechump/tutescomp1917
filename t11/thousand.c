#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

    if (argc < 4) {
        printf("Needs 3 parameters\n");
        return 1;
    }

    int start = strtol(argv[1], NULL, 10);
    int end = strtol(argv[2], NULL, 10);
    char * file = argv[3];

    FILE *fp = fopen(file, "w");
    int i;
    for (i = start; i < end; i++) {
        fprintf(fp, "%d ", i);
    }
    return 0;
}
