#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Needs a filepath!\n");
        return 1;
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Can't open file!\n");
        return 1;
    }
    
    int i = 0;
    char * line = malloc(sizeof(char) * 100);
    while (i < 10 && fgets(line, 100, fp) != NULL) {
        printf("%i: %s\n", i, line);
        i++;
    }
    return 0;
}
