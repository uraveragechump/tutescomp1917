#include <stdio.h>
#include <stdlib.h>

#define SIZE 6

double rMax(double * array, int size);
double rSum(double * array, int size);

int main() {
    double array[SIZE] = {10.0, 12.2, 14.3, 2.5, -3.2, 5.7};

    double sum = rSum(array, SIZE);
    printf("sum: %f\n", sum);

    double max = rMax(array, SIZE);
    printf("max: %f\n", max);
    return 0;

}










































/*
double rMax(double * array, int size) {
    if (size == 1) {
        return array[0];
    }
    double max = rMax(array, size-1);
    if (array[size-1] > max) {
        return array[size-1];
    }
    return max;
}

double rSum(double * array, int size) {
    if (size == 1) {
        return array[0];
    }
    return rSum(array, size-1) + array[size-1];
}
*/
