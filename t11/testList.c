#include <stdio.h>
#include <malloc.h>
#include "List.h"

#define TEST 1

// Modified from wk8 code
// Dong.

int main() {

    /*
    Lnode one = newNode(1);
    printf("%p\n", one);
    deleteNode(one, 1);
    Lnode two = newNode(2);
    printf("%p\n", two);
    */
    //Lnode three = newNode(3);
    //

    if (TEST == 0) {
        Lnode head = newNode(1);
        //printf("%p\n", head);
        Lnode new = newNode(3);
        Lnode last = newNode(4);

        // This line sets "head" at the front of the list after prepending new onto list head.
        printf("prepend 3 onto list (1)->(x), resets head\n");
        head = prepend(head, new);
        // Print's the list - these two lines will have diff output if head is not set properly
        //printList(new);
        printf("Size: %d\n",rLength(head));
        printList(head);

        // This line doesn't, so new is the new front of the list.
        //printf("prepend 3 onto 1, doesn't reset head\n");
        //prepend(head, new);

        // Appends last to end of list pointed to by head
        printf("append 4 onto list (3)->(1)->(x), head doesn't need to be reset\n");
        append(head, last);
        printf("Size: %d\n",rLength(head));
        printList(head);

        printf("delete the first 3 in list (3)->(1)->(4)->(x), resets head\n");
        head = deleteNode(head, 3);
        printf("Size: %d\n",rLength(head));
        printList(head);

        printf("delete the first 4 in list (1)->(4)->(x), resets head\n");
        head = deleteNode(head, 4);
        printf("Size: %d\n",rLength(head));
        printList(head);

        printf("delete the first 1 in list (1), resets head\n");
        head = deleteNode(head, 1);
        printf("Size: %d\n",rLength(head));
        printList(head);

    }
    else if (TEST == 1) {

        Lnode head = newNode(1);
        Lnode new = newNode(3);
        Lnode last = newNode(4);

        // This line sets "head" at the front of the list after prepending new onto list head.
        printf("prepend 3 onto list (1)->(x), resets head\n");
        head = prepend(head, new);
        // Print's the list - these two lines will have diff output if head is not set properly
        //printList(new);
        printf("Size: %d\n",rLength(head));
        rPrintRev(head);

        // This line doesn't, so new is the new front of the list.
        //printf("prepend 3 onto 1, doesn't reset head\n");
        //prepend(head, new);

        // Appends last to end of list pointed to by head
        printf("append 4 onto list (3)->(1)->(x), head doesn't need to be reset\n");
        append(head, last);
        printf("Size: %d\n",rLength(head));
        rPrintRev(head);

        printf("delete the first 3 in list (3)->(1)->(4)->(x), resets head\n");
        head = deleteNode(head, 3);
        printf("Size: %d\n",rLength(head));
        rPrintRev(head);

        printf("delete the first 4 in list (1)->(4)->(x), resets head\n");
        head = deleteNode(head, 4);
        printf("Size: %d\n",rLength(head));
        rPrintRev(head);

        printf("delete the first 1 in list (1), resets head\n");
        head = deleteNode(head, 1);
        printf("Size: %d\n",rLength(head));
        rPrintRev(head);
    }

    return 0;
}

