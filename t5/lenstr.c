#include <stdlib.h>
#include <stdio.h>

int len_str(char * str);

int main(int argc, char * argv[]) {
    if (argc != 2) {
        printf("Usage: ./lenstr <string>\n");
        return EXIT_FAILURE;
    }

    printf("Length of string \"%s\" is %d\n", argv[1], len_str(argv[1]));

    return EXIT_SUCCESS;

}

int len_str(char * str) {
    int i;
    for (i = 0; *(str+i) != '\0'; i++) {
        // Do absolutely nothing???
    }

    return i;
}
