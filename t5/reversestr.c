#include <stdio.h>
#include <stdlib.h>

void swap(char * ch1, char * ch2) {
    char temp = *ch1;
    *ch1 = *ch2;
    *ch2 = temp;
}

int len_str(char * str) {
    int i;
    for (i = 0; str[i] != '\0'; i++) {

    }
    return i;
}

void reverse_str(char s[]) {
    int i;
    int len = len_str(s);
    for (i = 0; i < len/2; i++) {
        int j = len-i-1;
        swap(&s[i], &s[j]);
    }
}

void reverse_strp(char * str) {
   int i;
   int len = len_str(str);
   for (i = 0; i < len/2; i++) {        // nuance with len/2
       char * p = str + i;
       char * q = str + len - i - 1;
       swap(p, q);
   }
}

int main(int argc, char** argv) {
    //char * str = "tesb eht si gnoD";
    
    //str2 = str;
    reverse_strp(argv[1]);
    printf("%s\n", argv[1]);
    return 0;
}

