#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int lenstr(char s[]) {
    int i;
    for (i = 0; s[i] != '\0'; i++) {
        
    }
    return i;
}

char * str_str(char s[], char d[]) {
    int sIndex, dIndex = 0;
    int len = lenstr(s);
    char * pointer;

    for (sIndex = 0; sIndex < len && d[dIndex] != '\0'; sIndex++) {
        printf("%d: s[sIndex]: {%c} - {%c} : d[dIndex]\n", dIndex, s[sIndex], d[dIndex]);
        if (s[sIndex] != d[dIndex]) {
            sIndex -= dIndex;
            dIndex = 0;
            pointer = NULL;
            continue;
        }
        else if (pointer == NULL) {
            pointer = s+sIndex;
            dIndex++;
        }
        else {
            dIndex++;
        }
    }

    //printf("{%c}\n", *pointer);
    return pointer;
}


int main(int argc, char** argv) {

    char * orig = malloc (sizeof(char) * 100);
    char * part = malloc (sizeof(char) * 100);

    fgets(orig, 100, stdin);
    fgets(part, 100, stdin);

    orig = strtok(orig, "\n");
    part = strtok(part, "\n");

    printf("%s\n", orig);

    char * extracted = str_str(orig, part);
    if (extracted == NULL) {
        printf("NULL");
    }
    else { 
        printf("%s\n", extracted);
    }

    return 0;
}
