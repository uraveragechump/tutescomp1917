#include <stdio.h>

int findIndex(int x, int a[], int N) {
    // Aim to get smallest index that contains x
    // Note that if we scan left to right, the first index will be the smallest.

    int i;
    for (i = 0; i < N; i++) {
        if (a[i] == x) {
            return i;
        }
    }
    return -1;
}

int main() {
    
    int array[10] = {2, 4, 7, 9, 54, -99, 54, 39, 0, 1};
    printf("Index of 54: %d\n", findIndex(54, array, 10));
    return 0;
}
