#include <stdio.h>

double max_valP(int n, double a[]) {
    double * max = a;

    int i;
    for (i = 1; i < n; i++) {
        if (a[i] > *max) {
            *max = *(a+i);
        }
    }
    return *max;

}

double max_val(int n, double a[]) {
    double max = a[0];

    int i;
    for (i = 1; i < n; i++) {
        if (a[i] > max) {
            max = a[i];
        }
    }
    return max;
}

int main() {
    double array[10] = {2.3, 4.5, 7.5, 9.2, 10.0, -99.1, 72.2, 39.3, 0.001, 1.0/3.0};
    printf("Max Val: %.3lf\n", max_val(10, array));
    printf("Max Val: %.3lf\n", max_valP(10, array));
    return 0;
}
