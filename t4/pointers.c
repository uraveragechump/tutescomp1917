#include <stdio.h>


void swaptests();

void swap(int * first, int * second) {
    int temp = *first;
    *first = *second;
    *second = temp;
}

// This is bad because C passes by value
void badswap(int first, int second) {
    int temp = first;
    first = second;
    second = temp;
}

void swaptests() {
    int x = 1;
    int y = 2;

    printf("Before Swap, (%d, %d)\n", x, y);

    // Swap statement. Note the ampersand
    swap(&x, &y);       

    printf("After Swap, (%d, %d)\n", x, y);

    x = 1;
    y = 2;

    printf("Before bad swap, (%d, %d)\n", x, y);

    // Swap statement. Note the ampersand
    badswap(x, y);       

    printf("After bad swap, (%d, %d)\n", x, y);
}

int main() {

    swaptests();
    
    int         n;
    int *       p;
    int *       q;
    double      x;
    double *    r;


    // p, q, r are pointers

    printf("&n: %20p, n: %20d\n", &n, n);
    printf("p: %20p, *p: %20d\n", p, *p);
    printf("q: %20p, *q: %20d\n", q, *q);
    printf("&x: %20p, x: -NAN-\n", &x, x);
    printf("r: %20p, *r: -NAN-\n", r);

    printf("-------START--------\n");
    
    p = &n;         
    
    printf("a. p: %20p, *p: %20d\n", p, *p);
    
    *p =  5;        

    printf("b. p: %20p, *p: %20d\n", p, *p);
    printf("b. n: %20d, *n: -NAN-\n", n);

    *q = 17;
    
    printf("c. q: %20p, *q: %20d\n", q, *q);

    q =  p;

    printf("d. p: %20p, *p: %20d\n", p, *p);
    printf("d. q: %20p, *q: %20d\n", q, *q);
    printf("d. n: %20d, *n: -NAN-\n", n);

    *q =  8;

    printf("e. p: %20p, *p: %20d\n", p, *p);
    printf("e. q: %20p, *q: %20d\n", q, *q);
    printf("e. n: %20d, *n: -NAN-\n", n);

    r = &x;

    printf("f. x: %20lf, *x: -NAN-\n", x);
    printf("f. r: %20p, *r: %20lf\n", r, *r);

    *r = 3.0;

    printf("g. x: %20lf, *x: -NAN-\n", x);
    printf("g. r: %20p, *r: %20lf\n", r, *r);

    *p = *r;

    printf("h. p: %20p, *p: %20d\n", p, *p);
    printf("h. r: %20p, *r: %20lf\n", r, *r);

    p =  r;     

    printf("i. p: %20p, *p: %20d\n", p, *p);
    printf("i. r: %20p, *r: %20lf\n", r, *r);

    *p =  n;

    printf("j. p: %20p, *p: %20d\n", p, *p);
    printf("j. n: %20d, *n: -NAN-\n", n);

    n =  x;    

    printf("k. n: %20d, *n: -NAN-\n", n);
    printf("k. x: %20lf, *x: -NAN-\n", x);

    return 0;
}
