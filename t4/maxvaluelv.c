#include <stdio.h>

double min_val(int n, double array[]) {
   int i;
   double min_val = array[0];
//   double min_val = 0;
   for (i = 1; i < n; i++) {
       if (min_val > array[i]) {
           min_val = array[i];
       }
   }
   return min_val;
}

int main() {
    double array[10] = {2.3, 4.5, -7.5, 9.2, 10.0, -99.1, 72.2, 39.3, 0.001, 1.0/3.0};
    printf("Max Val: %.3lf\n", min_val(10, array));
    //printf("Max Val: %.3lf\n", max_valP(10, array));
    return 0;
}
