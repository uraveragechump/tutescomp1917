#include <stdio.h>

void printRow(int row);

int main() {

    int table[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    int i;
    for (i = 0; i < 3; i++) {
        printRow(i);
        printf("\n");
    }   

    return 0;

}

void printRow(int row, int[][3] table) {
    int i;
    for (i = 0; i < 3; i++) {
        printf("%d, ", table[row][i]);
    }
}
