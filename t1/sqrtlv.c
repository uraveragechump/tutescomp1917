#include <stdio.h>
#include <math.h>

// compile with gcc -lm sqrt.c
int main(int argc, char** argv) {

    double input;
    printf("Enter a Number: ");
    scanf("%lf", &input);

    double output = sqrt(input);
    printf("The sqrt of %f is %f\n", input, output);

    return 0;

}

