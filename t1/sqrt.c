#include <stdio.h>
#include <math.h>

// compile with gcc -lm sqrt.c
int main(int argc, char** argv) {

    printf("Input a number: \n");
    double input;
    scanf("%lf", &input);

    double output = sqrt(input);

    if (input >= 0) {
        printf("The square root of %.2f is %.2f\n", input, output);
    }
    else {
        printf("No real square root of %.2f\n", input);
    }

    // Cube Rt
    printf("Input another number: \n");
    scanf("%lf", &input);
    output = pow(input, 1.0/3.0);

    if (input >= 0) {
        printf("The cube root of %f is %f\n", input, output);
    }
    else {
        printf("No real cube root of %f\n", input);
    }

    return 0;

}

