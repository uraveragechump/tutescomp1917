#include <stdio.h>
#include <math.h>

int main(int argc, char** argsv) {
    double a, b, c;
    scanf("%lf %lf %lf", &a, &b, &c);
    double s = (a + b + c)/2;
    double area = sqrt(s*(s-a)*(s-b)*(s-c));
    printf("Area: %f\n", area);
    return 0;
}
