#include <stdio.h>

int isdigit (int ch) {
    if (ch >= 48 && ch <= 57) {
        return 1;
    }
    else {  // Note that the else isn't necessary, although it might provide readability
        return 0;
    }
}

int islower (int ch) {

    // 'a' == 97
    if (ch >= 'a' && ch <= 'z') {
        return 1;
    }
    return 0;
}

int toupper (int ch) {

    // Best way
    if (ch >= 'a' && ch <= 'z') {
        return ch + 32;
    }
    return ch;
    // If you didn't know/forgot the gap between uppercase/lowercase
    /*
    if (ch >= 'a' && ch <= 'z') {
        return ch + 'a' - 'A';
    }
    return ch;
    */

    // If you didn't know anything about ASCII part 1 (Please don't do this)
    /*
    if (ch == 'a') {
        return 'A';
    } 
    else if (ch == 'b') {
        return 'B';
    }
    if (ch == 'c') {    // intentionally omitted the "else" to demonstrate that it logically means the same thing
        return 'C';
    }
    */
    // ... goes on

    // If you didn't know anything about ASCII part 2 (Better way to do this) (still relies on knowledge that b is indexed after a
    /*
    int arr[26] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    if (ch >= 'a' && ch <= 'z') {
        return arr[ch - 'a'];
    }
    return ch;
    */
}
int main() {
    char ch[] = {'a', 'b', 'C', 'Z', '1', 1};

    char num1 = '2';
    char char1 = 'a';

    printf("%d\n", isdigit(num1));
    printf("%d\n", isdigit(char1));

    int i = 0;

    // Last character printed is a strange symbol: What's happening here?
    for (i = 0; i < 6; i++) {
        printf("%c -> %c\n", ch[i], toupper(ch[i]));
    }
    return 0;
}
