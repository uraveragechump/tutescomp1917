#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

double bintodec(char* bin, int len) {
    int i;
    double sum = 0.0;
    double multiple = 0.5;
    int pt = len-1;
    
    // loop through string, starting from last character
    for (i = len-1; i >= 0; i--) {
        // If it's a decimal point, then mark where the point is, don't increment multiplier
        if (bin[i] == '.') {
            pt = i;
            continue;
        }

        // Otherwise increment multiplier, check if 0 or 1

        multiple = multiple * 2;
        // if 0, don't add. If 1, add 
        if (bin[i] == '0') {
            continue;
        }
        else { // bin[i] == '1'
            sum = sum + multiple;
        }
    }

    // We can express any binary number with a decimal pt in it in scientific notation
    // i.e. 1011.110 = 1011110 * 2^-3 (or 1011110 / 2^3)
    int power = len-pt-1;
    sum = sum/pow(2, power);
    return sum;
}

double basetodec(int base, char* number, int len) {
    int i;
    double sum = 0.0;
    double multiple = 1.0/(double) base;
    int pt = len-1;
//    int ptr = number + len-1;
    
    // loop through string, starting from last character
    for (i = len-1; i >= 0; i--) {
        // If it's a decimal point, then mark where the point is, don't increment multiplier
        if (number[i] == '.') {
            pt = i;
            continue;
        }

        // Otherwise increment multiplier, check which digit it is
        multiple = multiple * base;
        int digit;
        for (digit = 0; digit < base; digit++) {
            char c = number[i];
            if (atoi(&c) == digit) {
                sum = sum + (multiple * digit);
                break;
            }
        }
        //ptr--;
    }

    // We can express any binary number with a decimal pt in it in scientific notation
    // i.e. 1011.110 = 1011110 * 2^-3 (or 1011110 / 2^3)
    int power = len-pt-1;
    sum = sum/pow(base, power);
    return sum;
}

int main() {
    char* str = "1011.110";
    char* str2 = "1011";
    char* str3 = "5342";
    printf("%s -> %.9lf\n", str, bintodec(str, 8));
    printf("%s -> %.9lf\n", str2, bintodec(str2, 4));
    printf("%s -> %.9lf\n", str, basetodec(7, str, 8));
    printf("%s -> %.9lf\n", str2, basetodec(7, str2, 4));
    printf("%s -> %.9lf\n", str3, basetodec(7, str3, 4));
    return 0;
}
