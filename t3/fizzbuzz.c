#include <stdio.h>


void fizzbuzz(int num) {
    if (num % 3 != 0 && num % 5 != 0) {
        printf("%d", num);
    }

    if (num % 3 == 0) {
        printf("Fizz");
    }
    if (num % 5 == 0) {
        printf("Buzz");
    }
    printf("\n");

}

int main() {
    int i;
    for (i = 1; i <= 100; i++) {
        fizzbuzz(i);
    }
    return 0;

}
