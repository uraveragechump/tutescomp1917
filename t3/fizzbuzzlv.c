#include <stdio.h>
#include <stdlib.h>

int main() {

    int i;
    for (i = 0; i <= 100; i++) {
        
        // if i is divisible by 3 then do things
        if (i % 3 == 0) {
            printf("Fizz");
        } 

        if (i % 5 == 0) {
            printf("Buzz");
        }
    
        // It's not divisible by 3 or 5
        if(i % 3 != 0 && i % 5 != 0) {
            printf("%d", i);
        }

        printf("\n");

    }

    return EXIT_SUCCESS; // Exactly the same as return 0;

}
