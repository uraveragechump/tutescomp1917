#include <stdio.h>

int main() {
    int i = 0;
    char str[8]; // gcc will allocate 12 bytes for this (gcc is weird like that)

    // inputting a string of more than 12 characters will overflow
    scanf("%s", str);

    if (i != 0) {
        printf("hacks\n");
    } 
    else {
        printf("finish\n");
    }

}
