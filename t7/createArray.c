#include <stdio.h>
#include <stdlib.h>

int * createArray(int);

int main(int argc, char *argv[]){
    int size = 0;
    scanf("%d", &size);

    int *arr = createArray(size);

    // populate array with 1, 2, 3... size-1
    int i;
    for (i = 0; i < size; i++) {
        arr[i] = i;
    }

    // print array
    for (i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }   
    return 0;
}

int *createArray(int size){
    int new_array[size];
    return new_array;
}
