#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct point Point;

struct point {
    double x; 
    double y;
};

Point * newPoint(double x, double y) {
    Point * p = malloc (sizeof(Point));
    (*p).x = x;
    p->y = y;
    return p;
}

double distA(double p[2], double q[2]) {
    return sqrt(pow(p[0] - q[0], 2) + pow(p[1] - q[1], 2));
}

double distS(Point p, Point q) {
    return sqrt(pow(p.x - q.x, 2) + pow(p.y - q.y, 2));
}

int main() {
    double pA[2] = {0, 0};
    double qA[2] = {4, 3};

    Point * p; 
    Point * q;
    Point pS, qS;

    p = newPoint(0, 0);
    q = newPoint(4, 3);

    pS = *p;
    qS = *q;

    printf("Array:  Distance between points (%.1lf, %.1lf) and (%.1lf, %.1lf) is %.1lf\n", pA[0], pA[1], qA[0], qA[1], distA(pA, qA));
    printf("Struct: Distance between points (%.1lf, %.1lf) and (%.1lf, %.1lf) is %.1lf\n", pS.x, pS.y, qS.x, qS.y, distS(pS, qS));

    return 0; 
}
