/*
 * Two iterations of readArray, one is much nicer using scanf (readArrayS)
 * The other one scans character by character - and only returns 1 digit numbers (readArray)
 * readArray can probably be extended to take in multi-digit numbers, but not demonstrated here.
 *
 * Dong.
 */

#include <stdio.h>
#include <stdlib.h>

int * readArray(int);
int * readArrayS(int);

int main() {
    int size = 0;
    scanf("%d", &size);
    int * arr = readArrayS(size);

    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }

    free(arr);

    return 0;
}

int * readArray(int size) {
    int * thisArray = malloc (sizeof(int) * size);
    int c;
    int i = 0;
    while ((c = getchar()) != EOF && i < size) {
        if (c >= '0' && c <= '9') {
            thisArray[i++] = c - '0';
        }
    }
    return thisArray;
}

int * readArrayS(int size) {
    int * thisArray = malloc (sizeof(int) * size);
    int i;
    for (i = 0; i < size; i++) {
        scanf("%d ", &thisArray[i]);
    }
    return thisArray;
}
