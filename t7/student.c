#include <stdio.h>
#include <stdlib.h>

#define MAX 1000

typedef struct student Student;

struct student {
    int zid;
    double height;
};

Student * newStudent(int zid, double height) {
    Student * s =  malloc (sizeof(Student));
    s->zid = zid;
    s->height = height;
    return s;
}

int main() {

    int size = 0;
    int zid;
    double height;
    Student * stuArray = malloc (sizeof(Student) * MAX);

    printf("Input format:\n<zid>, <height>\n");

    // scanf returns a positive number when successful, negative when unsuccessful
    while (scanf("%d, %lf", &zid, &height) > 0 && size < MAX) {
        Student * s = newStudent(zid, height);
        stuArray[size++] = *s;
    }

    stuArray = realloc(stuArray, sizeof(Student) * size);

    int i;
    for (i = 0; i < size; i++) {
        printf("Student zID:    %07d\n", stuArray[i].zid);
        printf("Student Height: %.2f\n", stuArray[i].height);
    }
    return 0;
}
