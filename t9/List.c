#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#include "List.h"

List newList() {
    List l = malloc(sizeof(List));
    l->head = NULL;
    l->numNodes = 0;
    return l;
}

void append(List l, Lnode new) {

    if (new == NULL || l == NULL) {
        printf("died in Append\n");
        exit(1);
    }

    // Appending to new list - make the node the head
    if (l->head == NULL) {
        l->head = new;
        l->numNodes++;
        return;
    }

    Lnode curr = NULL;
    Lnode prev = NULL;

    // Else appending to end of list, find end
    /*
    for (curr = l->head; curr != NULL; curr = curr->next) {
        prev = curr;
    }
    */
    while (1) {
        prev = curr;
        curr = curr->next;
    }

    prev->next = new;
    l->numNodes++;

}

void printList(List l) {

    // Added a size print - because it's relevant info about the list
    printf("Size: %d\n", l->numNodes);
    Lnode curr;

    for (curr = l->head; curr != NULL; curr = curr->next) {
        printf("(%d)->", curr->val);
    }

    printf("(x)\n");
}

// Go through code improvements if you remember (reduce repeat code and inverted logic)
void deleteNode(List l, int val) {
    Lnode curr = NULL;
    Lnode prev = NULL;

    for (curr = l->head; curr != NULL; curr = curr->next) {

        // Do things if you find the first value
        if (curr->val == val) {

            if (prev == NULL) {
                //we got the first value
                l->head = curr->next;
            }
            else if (curr->next == NULL) {
                //We got the last value
                prev->next = NULL;
            }
            else {
                //We got a middle value
                prev->next = curr->next;
            }

            // We need to reduce numNodes, regardless of which one we removed
            curr = NULL;
            free(curr);
            l->numNodes--;
            return;
        }
        prev = curr;
    }
}

int getVal(List l, int index) {

    if (index >= l->numNodes) {
        return -1;
    }
    int i;
    Lnode curr = l->head;
    for (i = 0; i < index && curr != NULL; i++) {
        curr = curr->next;
    }
    if (curr == NULL) {
        exit(1);
    }
    return curr->val; 
}

void insertAt(List l, Lnode n, int index) {
    if (index > l->numNodes) {
        append(l, n);
        return;
    }

    int i = 0;
    Lnode curr = l->head;

    for (i = 0; i < index; i++) {
        curr = curr->next;
    }
    n->next = curr->next;
    curr->next = n;
    l->numNodes++;
}

void shuffle_merge(List l, List m) {

    Lnode mprev = NULL;

    Lnode lcurr = l->head;
    Lnode mcurr = m->head;

    Lnode lnext = lcurr->next;
    Lnode mnext = mcurr->next;

    // Loop until we reach the end of either list
    // Use the zip diagram!
    while (lcurr != NULL && mcurr != NULL) {
        lnext = lcurr->next;
        mnext = mcurr->next;

        lcurr->next = mcurr;
        mcurr->next = lnext;

        mprev = mcurr;

        lcurr = lnext;
        mcurr = mnext;
    }

    // Fix up the end
    if (m->numNodes > l->numNodes) {
        mprev->next = mcurr;
    }

    l->numNodes += m->numNodes;

}

// Haven't fully tested - test it yourself and see where it goes wrong!
List shuffle_merge_l(List l, List m) {
    List shuffled = newList();
    int i = 0, j = 0, k = 0;
    for (i = 0; j < l->numNodes || k < m->numNodes; i++) {
        printf("%d %d %d\n", i, j, k);
        Lnode insert = NULL;
        int val = -1;
        if (i % 2 == 0 && j < l->numNodes) {
            val = getVal(l, j);
            insert = newNode(val);
            append(shuffled, insert);
            j++;
        }
        else if (k < m->numNodes) {
            val = getVal(m, k);
            insert = newNode(val);
            append(shuffled, insert);
            k++;
        }
    }
    return shuffled;
}
/*
void reverse(List l) {
    Lnode curr = l->head;
   
}
*/
Lnode newNode(int val) {
    Lnode new = malloc(sizeof(Lnode));
    new->val = val;
    new->next = NULL;
    return new;
}
