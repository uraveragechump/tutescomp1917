#include <stdio.h>

#include "List.h"

#define TEST 2

int main() {

    if (TEST == 1) {

        Lnode two = newNode(2);
        Lnode three = newNode(3);

        List list = newList();
        append(list, three);
        append(list, two);

        printList(list);

        deleteNode(list, 3);
        deleteNode(list, 1);

        printList(list);
    }
    else if (TEST == 2) {

        Lnode one = newNode(1);
        Lnode two = newNode(2);
        Lnode three = newNode(3);
        Lnode four = newNode(4);
        Lnode five = newNode(5);
        Lnode six = newNode(6);
        Lnode seven = newNode(7);

        List first = newList();
        List second = newList();

        append(first, one);
        append(first, three);
        append(first, five);
        append(first, seven);

        append(second, two);
        append(second, four);
        append(second, six);
        //append(second, seven);

        printList(first);
        printList(second);

        //List shuffled = shuffle_merge_l(first, second);
        //printList(shuffled);
        shuffle_merge(first, second);
        printList(first);

    }
    else if (TEST == 3) {

    }

    
    return 0;
}
