typedef struct node* Lnode;
typedef struct list* List;

struct node {
    int val;
    Lnode next;
};

struct list {
    Lnode head;
    Lnode tail;
    int numNodes;

};

List newList();
void append(List l, Lnode new);
void printList(List l);
void deleteNode(List l, int val);

void shuffle_merge(List l, List m);
List shuffle_merge_l(List l, List m);
void reverse(List l);
void swap(List l);

Lnode newNode(int val);
