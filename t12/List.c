#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "List.h"

// Taken from wk8 tutorial code
// Dong.
/*
typedef struct node * Lnode;

struct node {
    int val;
    Lnode next;
}
*/
// Initialises a newNode with memory allocation and value-setting
Lnode newNode(int value) {
    Lnode newNode = malloc (sizeof(Lnode));
    if (newNode == NULL) { 
        printf("no memory\n");
        exit(1);
    }
    newNode->val = value;
    newNode->next = NULL;
    return newNode;
}

// Returns the head (front) of the new list
Lnode prepend(Lnode head, Lnode new) {
    new->next = head;
    return new;
}

// Returns the head (front) of the new list;
Lnode append(Lnode head, Lnode new) {
    if (head == NULL) {
        return new;
    }

    Lnode curr = head;

    // Last element of a list node will have ->next pointing nowhere (NULL)
    for (curr = head; curr->next != NULL; curr = curr->next) {
        // Do nothing, just set curr to last item
    }
    curr->next = new;
    //printf("[%d]->[%d]\n", curr->val, curr->next->val);
    return head;
}

// WhileLoop print
void printList(Lnode head) {
    int i = 0;
    Lnode curr = head;

    // Iterate through list until end is found (where curr == NULL)
    while (curr != NULL && i < 20) {
        printf("(%d)->", curr->val);
        curr = curr->next;
        i++;
    }

    printf("(x)\n");
}
// ForLoop print
void printListF(Lnode head) {
    Lnode curr = head;

    for (curr = head; curr != NULL; curr = curr->next) {
        printf("[%d]->", curr->val);
    }

    printf("(x)\n");
}
// Returns new head of list (always reallocate your head pointer to this return)
Lnode deleteNode(Lnode head, int val) {
    // Return nothing if nothing to delete
    if (head == NULL) {
        return NULL;
    }

    Lnode curr = head;
    Lnode prev = NULL;

    // Iterate through list, checking for values
    for (curr = head; curr != NULL; curr = curr->next) {
        if (curr->val == val) {
            // free node and create connection from prev to next
            // a. first item to be removed
            // b. only item to be removed
            // c. non-first item to be removed
            if (prev == NULL && curr->next != NULL) {
                /*
                head = head->next;
                curr = NULL;
                free(curr);
                */
                curr = head->next;
                head = NULL;
                free(head);
                return curr;
            }
            else if (prev == NULL && curr->next == NULL) {
                head = NULL;
                free(head);
                return NULL;
            }
            prev->next = curr->next;
            curr = NULL;
            free(curr);
            return head;
        }
        prev = curr;
    }
    
    // Removed nothing, return list
    return head;
    
}
// Strategy: Get the last node in the list with head L, and link it to head of M
// Exactly the same as append!
Lnode concat(Lnode L, Lnode M) {

    return append(L, M);
    /*
    Lnode curr = L;
    for (curr = L; curr->next != NULL; curr = curr->next) {
        // Do nothing, get curr to last node in list with head L
    }
    curr->next = M;
    return L;
    */
}

Lnode reverse(Lnode head) {
    Lnode prev = NULL;
    Lnode curr = head;
    Lnode next = curr->next;

    for (curr = head; curr != NULL; curr = next) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
    }
    return prev;
}

int rLength(Lnode head) {
    if (head == NULL) {
        return 0;
    }
    return rLength(head->next) + 1;
}
void recPrintRev(Lnode head) {
    if (head == NULL) {
        return;
    }
    recPrintRev(head->next);
    printf("<-(%d)", head->val);

}

void rPrintRev(Lnode head) {
    recPrintRev(head);
    printf("\n");
}

int sumOfTwo(Lnode head, int k) {
    Lnode curr1 = head;
    for (curr1 = head; curr1 != NULL; curr1 = curr1->next) {
        Lnode curr2 = curr1;
        for (curr2 = curr1; curr2 != NULL; curr2 = curr2->next) {
            if (curr2->val + curr1->val == k) {
                return 1;
            }
        }
    }
    return 0;
}
