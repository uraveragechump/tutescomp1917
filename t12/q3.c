#include <stdio.h>

#define LENGTH 6

int main() {
    int number[LENGTH] = {10, 21, -23, 45, -21, 201};
    int result = 0;
    int j = 0;
    while ( j < LENGTH) {
        if (number[j] < 0) {
            result = result + 1;
        }
        j++;
    }
    printf("%d\n", result);
    return 0;
}
