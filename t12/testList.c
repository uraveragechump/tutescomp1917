#include <stdio.h>
#include <malloc.h>
#include "List.h"

#define K 9

void printResult(Lnode head, int k);

// Modified from wk8 code
// Dong.

int main() {

    Lnode first = newNode(1);
    Lnode second = newNode(3);
    Lnode third = newNode(4);
    Lnode fourth = newNode(-2);
    Lnode fifth = newNode(5);

    append(first, second);
    append(first, third);
    append(first, fourth);
    append(first, fifth);

    printList(first);
    printResult(first, K);

    deleteNode(first, 5);

    printList(first);
    printResult(first, K);

    return 0;
}

void printResult(Lnode head, int k) {
    if (sumOfTwo(head, K)) {
        printf("Two numbers in the list sum up to %d\n", K);
    }
    else {
        printf("No two numbers in the list sum up to %d\n", K);
    }
}

