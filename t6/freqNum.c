// README
// Counts from stdin until EOF is found
// To test, type in characters and finish with "CTRL-D"
//
// Credits to Ka Hei :)

#include <stdio.h>

// Scans user input, counts into the count
void scanNums(int * count) {
    char c;
    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9') {
            int num = c - '0';
            count[num] = count[num] + 1;
        }
    }
}

void printFreq(int * count, int n) {
    int i = 0;
    while (i < n) {
        printf("%d %d\n", i, count[i]);
        i++;
    }
}

int main() {
    int count[10] = {0};

    scanNums(count);

    printf("\n");
    
    printFreq(count, 10);
    return 0;
}

