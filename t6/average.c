// Credits to Ka Hei :)

#include <stdio.h>

double average(int len, int a[]) {
    // Returns the average of the array a
    double sum = 0;
    int i = 0;
    while (i < len) {
        sum = sum + a[i];
        //printf("%d\n", a[i]);
        i++;
    }
    //printf("%d\n", sum);
    double avg = sum/len;
    return avg;

}

int main() {
    int a[] = {20, 35, -63, 42, 57};
    int len = 5;

    double avg = average(len, a);
    printf("%lf\n", avg);
    return 0;
}
