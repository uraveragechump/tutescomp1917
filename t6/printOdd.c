// READ:
//
// I've made two implementations, the second is commented out.
// 
// Anyone looking to improve their logic skills (hopefully all of you), take the time to try understand what's happening. :)
//
// To look at the second one in action:
// - uncomment all function headers
// - uncomment all functions (remove /* before getCharN, remove */ at bottom)
//
// Dong :)
//
// (there are probably better implementations out there - this was something I hacked quickly)

#include <stdio.h>
#include <stdlib.h>

#define MAX 1000

int scanN();
void printOdd(int n);
//int getCharN();
//int isDigit(int c);
//int charToNum(int c);
//int strToNum(char * str);

int main() {
    int n = scanN();
    //int n = getCharN();
    printOdd(n);
    return 0;
}

int scanN() {
    int n;
    scanf("%d", &n);
    return n;
}


void printOdd(int n) {
    int i;
    // i = 1 : we want to start printing from 1
    // i <= n : the last thing we possibly want to print is n
    // i += 2 : we're printing odd numbers, which are spaced two apart from each other
    for (i = 1; i <= n; i+=2) {
        printf("%d\n", i);
    }
}
/*
int getCharN() {
    char c;
    int i = 0;
    char * numStr = malloc (sizeof(char) * MAX);

    while ( isDigit((c = getchar())) && i < MAX - 1) {
        numStr[i] = c;
        i++;
    }
    numStr[i] = '\0'; // Terminate the string

    int n = strToNum(numStr); 
    return n;
}

int isDigit(int c) {
    if (c >= '0' && c <= '9') {
        return 1;
    }
    return 0;
}

int charToNum(int c) {
    if (isDigit(c)) {
        return c - '0';
    }
    return c;
}

int strToNum(char * numStr) {
    if (numStr == NULL) {
        printf("uninitialised string\n");
        exit(1);
    }
    int i;
    int num = 0;
    for (i = 0; numStr[i] != '\0'; i++) {
        // "add a zero to the end of the number"
        num *= 10;
        int digit = charToNum(numStr[i]);
        // replace the zero with the next digit
        num += digit;
    }
    if (i == 0) {
        printf("empty string, returning 0\n");
        return 0;
    }
    return num;
}
*/
