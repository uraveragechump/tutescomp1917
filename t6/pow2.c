#include <stdio.h>

int isPow2(int n) {
    
    // while n is even, divide by 2
    while (n % 2 == 0) {
        n = n/2;
    }
    if (n == 1) {
        return 1;
    }
    return 0;

}

int main(int argc, char** argv) {
    int n;
    scanf("%d", &n);
    if (isPow2(n)) {
        printf("%d is a power of 2\n", n);
    }
    else {
        printf("%d is not a power of 2\n", n);
    }
    return 0;
}
