// Copied & modified from t1 directory
// printTRTri offers a different approach to this problem
//
// Dong.

#include <stdio.h>
// Bottom Left Triangle (i.e. right angle at bottom left)
void printBLTri(int num) {

    int i, j;

    // Prints the rows (lowerlim;upperlim;inc)
    for (i = 0; i < num; i++) {

        int blTri = i + 1;

        // Prints the characters of each row
        for (j = 0; j < blTri; j++) {
            printf("*");
        }
        printf("\n");
    }
}
// Top Left Triangle (i.e. right angle at top left)
void printTLTri(int num) {

    int i, j;

    // Prints the rows (lowerlim;upperlim;inc)
    for (i = 0; i < num; i++) {

        int tlTri = num - i;

        // Prints the characters of each row
        for (j = 0; j < tlTri; j++) {
            printf("*");
        }
        printf("\n");
    }
}

// Top Right Triangle (i.e. right angle at top right)
void printTRTri(int num) {

    int i, j;
    for (i = 0; i < num; i++) {
        for (j = 0; j < num; j++) {
            if (j >= i) {
                printf("*");
            }
            else {
                printf(" ");
            }
        }
        printf("\n");
    }
}

// Bottom Right Triangle (i.e. right angle at bottom right)
void printBRTri(int num) {

    int i, j;

    // Prints the rows (lowerlim;upperlim;inc)
    for (i = 0; i < num; i++) {

        int trTri = num - i;
        //int brTri = i + 1;

        // Prints the characters of each row
        for (j = 0; j < trTri - 1; j++) {
            printf(" ");
        }
        for (; j < num; j++) {
            printf("*");
        }
        printf("\n");
    }

}

int main() {
    int num;
    printf("Enter n : ");
    scanf("%d", &num);
    printf(" === Bottom Left Triangle ===\n");
    printBLTri(num);
    printf(" === Bottom Right Triangle ===\n");
    printBRTri(num);
    printf(" === Top Left Triangle ===\n");
    printTLTri(num);
    printf(" === Top Right Triangle ===\n");
    printTRTri(num);
    return 0;

}
