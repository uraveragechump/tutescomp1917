#include <stdio.h>

int main() {
    int num;
    printf("Enter : ");
    scanf("%d", &num);

    int i = 0;
    while (i < num) {
        printf("*\n");
        i++; // i = i + 1;
    }
    return 0;
}
