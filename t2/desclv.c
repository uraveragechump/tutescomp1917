#include <stdio.h>

int main() {

    int first, second, third;
    printf("Enter numbers: ");
    scanf("%d %d %d", &first, &second, &third);

    if (first > second && second > third) {
        printf("In descending order\n");
    }
    else if (second > first) {
        printf("Second > First\n");
    }

    
    return 0;

}
