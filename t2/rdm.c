#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

    srandom(time(NULL));

    //produce number between 0 and 108
    int num1 = rand() % 108;
    int num2 = rand() % 108;

    printf("num1 : {%d}, num2 : {%d}\n", num1, num2);

    if (num2 < num1 && num1 % num2 == 0) {
        printf("Smaller is factor of larger\n");
    }
    else if (num1 < num2 && num2 % num1 == 0) {
        printf("Smaller is factor of larger\n");
    }
    else {
        printf("Nope\n");
    }
    return 0;
}
