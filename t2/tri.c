#include <stdio.h>


void printBTri(int num) {

    int i, j;

    // Prints the rows (lowerlim;upperlim;inc)
    for (i = 0; i < num; i++) {

        int blTri = i + 1;
        //int tlTri = num - i;

        // Prints the characters of each row
        for (j = 0; j < blTri; j++) {
            printf("*");
        }
        printf("\n");
    }
}

void printTRTri(int num) {

    int i, j;

    // Prints the rows (lowerlim;upperlim;inc)
    for (i = 0; i < num; i++) {

        int trTri = num - i;
        //int brTri = i + 1;

        // Prints the characters of each row
        for (j = 0; j < trTri; j++) {
            printf("*");
        }
        for (; j < num; j++) {
            printf(" ");
        }
        printf("\n");
    }

}

void printBRTri(int num) {

    int i, j;

    // Prints the rows (lowerlim;upperlim;inc)
    for (i = 0; i < num; i++) {

        int trTri = num - i;
        //int brTri = i + 1;

        // Prints the characters of each row
        for (j = 0; j < trTri - 1; j++) {
            printf(" ");
        }
        for (; j < num; j++) {
            printf("*");
        }
        printf("\n");
    }

}

int main() {
    int num;
    printf("Enter n : ");
    scanf("%d", &num);

    printBTri(num);
    printf("Top Tri: \n");
    printTRTri(num);
    return 0;

}
