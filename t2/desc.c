#include <stdio.h>

int main(int argc, char** argv) {
    int first, second, third;
    printf("Enter numbers: ");
    scanf("%d %d %d", &first, &second, &third);

    if (first > second && second > third) {
        printf("In descending order.\n");
    }
    else if (first < second && second < third) {
        printf("In ascending order.\n");
    }
    else {
        printf("Neither ascending/descending\n");
    }
    return 0;

}
